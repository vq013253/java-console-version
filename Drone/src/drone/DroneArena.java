package drone;

import java.util.ArrayList;
import java.util.Random;

public class DroneArena {
	int yAxis, xAxis;
	int ymax;
	int xmax;
	ArrayList<Drone> manyDrones;
	Drone d;
	Random randomGenerator;

	public DroneArena(int width, int height) {

		xAxis = width;
		yAxis = height;
		randomGenerator = new Random();
		manyDrones = new ArrayList<Drone>();

	}
	/*
	 * the get functions return attributes of the arena
	 */

	public int getX() {

		return this.xAxis;
	}

	public int getY() {

		return this.yAxis;
	}

	public String toString() {
		String arena = "The arena size " + yAxis + " " + xAxis + "\n";
		for (int i = 0; i < manyDrones.size(); i++) {
			arena += manyDrones.get(i).toString();
		}
		return arena;

	}

	public void showDrones(ConsoleCanvas c) {
		for (Drone d : manyDrones) {
			d.displayDrone(c);
		}
	}

	public void moveAllDrones() {
		for (Drone d : manyDrones) {
			d.tryToMove(this);
		}

	}

	public Drone getDroneAt(int x, int y) {
		Drone a = null;
		for (Drone d : manyDrones) {
			if (d.isHere(x, y) == true) {
				return a = d;
			} else {
				return a;
			}
		}
		return a;

	}
	/*
	 * checks if there is drone in the position it is given and then displays error
	 * if the arena is full or  if there is another drone in that position
	 * 
	 */

	public void addDrone() {
		int x, y;

		if (manyDrones.size() < xAxis * yAxis) {
			do {
				x = randomGenerator.nextInt(xAxis);
				y = randomGenerator.nextInt(yAxis);
			} while (getDroneAt(x, y) != null);

			Direction direction = Direction.randomDirection();
			d = new Drone(x, y, direction);
			manyDrones.add(d);

		} else if (!manyDrones.isEmpty()) {
			System.err.println("\nThe arena is already full!\n");
		} else {
			System.err.println("\nThe arena does not exist");
		}

	}

	public boolean canMoveHere(int x, int y) {
		if (getDroneAt(x, y) != null || x >= xAxis || y >= yAxis || x < 0 || y < 0)
			return false;

		return true;
	}

}

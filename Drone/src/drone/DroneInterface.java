package drone;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import javax.swing.JFileChooser;

public class DroneInterface {

	private Scanner s;
	private DroneArena myArena;

	public DroneInterface() {
		s = new Scanner(System.in);
		myArena = new DroneArena(10, 10);

		char ch = ' ';
		do {
			System.out.print(
					"Enter: \n(A)Add drone \n(I)Get information \n(D)Display arena and drones\n(M)Move drones once "
							+ "\n(W)Move drones 10 times\n(N)New arena dimensions\n(F)File load/save\n(X)Exit\n");
			ch = s.next().charAt(0);
			s.nextLine();
			switch (ch) {
			case 'A':
			case 'a':
				myArena.addDrone(); // add a new drone to arena
				break;
			case 'I':
			case 'i':
				System.out.print(myArena.toString());
				break;
			case 'D':
			case 'd':
				doDisplay();
				break;
			case 'M':
			case 'm':
				myArena.moveAllDrones(); // Move all drones
				doDisplay(); // Display new arena with moved drones
				break;
			case 'W':
			case 'w':
				if (myArena.manyDrones.isEmpty() == false) {
					for (int i = 0; i < 10; i++) {
						System.out.println("-----------------------------------");
						myArena.moveAllDrones();
						doDisplay();
						System.out.print(myArena.toString());
						try {
							TimeUnit.MILLISECONDS.sleep(200); // Wait for 200ms
						} catch (InterruptedException e) {
							System.err.format("IOException: %s%n", e);
						}
					}
				} else {
					System.out.println("No drones to move!"); // if list is empty there are no drones to move
				}
				break;
			case 'N':
			case 'n':
				int x, y;
				String input = "";
				System.out.print("Enter new Arena's Y value: ");
				input = s.next();
				x = Integer.parseInt(input);
				s.nextLine();
				System.out.print("Enter new Arena's X value: ");
				input = s.next();
				y = Integer.parseInt(input);
				s.nextLine();
				myArena = new DroneArena(x, y);
				System.out.println("\nArena successfully created with Y = " + x + " and X = " + y);
				break;
			case 'F':
			case 'f':
				fileOptions(); // opens file options menu
				break;
			case 'x':
				ch = 'X';
				break;
			}
		} while (ch != 'X');

		s.close();
	}

	void doDisplay() {
		if (myArena.getX() > 0 && myArena.getY() > 0) {
			ConsoleCanvas canvas = new ConsoleCanvas(myArena.getX() + 2, myArena.getY() + 2);
			myArena.showDrones(canvas);
			System.out.println(canvas.toString());
			if (myArena.manyDrones.isEmpty()) {
				System.out.println("\nNo drones added yet!");
			}
		} else {
			System.out.println("\nArena does not exist!");
		}

	}

	void saveFile() throws IOException {

		// JFileChooser properties
		JFileChooser chooser = new JFileChooser("D:\\files\\");// directory
		chooser.setDialogTitle("Save arena to: "); // Window title
		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES); // What files are shown
		// this where the windows opens
		int returnVal = chooser.showOpenDialog(null); // stores if user clicks open/cancel
		if (returnVal == JFileChooser.APPROVE_OPTION) { // if user presses open
			File userFile = chooser.getSelectedFile(); // gets the file selected by user
			System.out.println("Arena saved!\n" + "File Name: " + userFile.getName() + "\nDirectory: "
					+ userFile.getAbsolutePath()); // prints file chosen and directory
			// Saving Process
			FileWriter fileWriter = new FileWriter(userFile); // creates a new file writer
			BufferedWriter writer = new BufferedWriter(fileWriter); // adds to buffer
			// First saves the arena dimensions on first line
			writer.write(Integer.toString(myArena.getX()));
			writer.write(" ");
			writer.write(Integer.toString(myArena.getY()));
			writer.newLine(); // change line
			// Each line store one drone in the form X Y DIRECTION
			for (Drone d : myArena.manyDrones) {
				writer.write(Integer.toString(d.gety()));
				writer.write(" ");
				writer.write(Integer.toString(d.getx()));
				writer.write(" ");
				writer.write(Integer.toString(d.getDir().ordinal()));
				writer.newLine();
			}
			writer.close();
		}
	}

	void loadFile() throws IOException {
		// JFileChooser properties
		JFileChooser chooser = new JFileChooser("D:\\files");// directory
		chooser.setDialogTitle("Load arena from: ");// Window title
		chooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);// What files are shown
		// This will eventually store the file contents
		String fileContents = " ";
		// This where the windows opens
		int returnVal = chooser.showOpenDialog(null); // stores if user clicks open/cancel
		if (returnVal == JFileChooser.APPROVE_OPTION) {// if user presses open
			File userFile = chooser.getSelectedFile(); // gets the file selected by user
			if (chooser.getSelectedFile().isFile()) { // if the file exists
				System.out.println("Arena Loaded!\n" + "File Name: " + userFile.getName() + "\nDirectory: "
						+ userFile.getAbsolutePath());// prints file chosen and directory

				// Clear the current drone list
				if (!myArena.manyDrones.isEmpty()) {
					myArena.manyDrones.clear();
				}
				// Loading process
				FileReader fileReader = new FileReader(userFile);
				BufferedReader reader = new BufferedReader(fileReader);

				fileContents = reader.readLine();
				String[] loadSize = fileContents.split(" "); // Store file contents separated by spaces in array
				int loadX = Integer.parseInt(loadSize[0]); // First integer is arena X dimension
				int loadY = Integer.parseInt(loadSize[1]); // Second integer is arena Y dimension
				myArena = new DroneArena(loadX, loadY); // creates a new arena with the gathered dimensions
				while (fileContents != null) { // while not in the end of the file
					fileContents = reader.readLine();
					String[] numbers = fileContents.split(" ");
					int x = Integer.parseInt(numbers[0]); // First integer is drone X coordinate
					int y = Integer.parseInt(numbers[1]); // Second integer is drone Y coordinate
					int ordinal = Integer.parseInt(numbers[2]); // Third integer is drone facing Direction
					// creates drone and adds it do list
					myArena.manyDrones.add(new Drone(x, y, Direction.values()[ordinal]));

				}
				reader.close();
			}

		}
	}

	void fileOptions() {// file menu
		s = new Scanner(System.in); // scanner
		char ch = ' ';
		System.out.print("Enter: " + "\n(S)ave File" + "\n(L)oad File" + "\n(X)Exit >");
		ch = s.next().charAt(0);
		s.nextLine();
		switch (ch) {
		case 'S':
		case 's':
			try {
				saveFile();
			} catch (Exception e) {
				System.err.print(" ");// error message
			}
			break;
		case 'L':
		case 'l':
			try {
				loadFile();
			} catch (Exception a) {
				System.err.print(""); // error message
			}

			break;
		case 'x':
			ch = 'X';
			break;
		default:
			break;
		}

	}
	/*
	 * The user can choose where to save their build, with JFileChooser and store it
	 * as text file
	 * 
	 * the method has exceptions so when something goes wrong an error pops up
	 * 
	 * there are no filters so the user can chose freely what file they want
	 */

	public static void main(String[] args) {
		DroneInterface r = new DroneInterface();

	}

}
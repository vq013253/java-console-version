package drone;

public class ConsoleCanvas {
	private char[][] canvas;
	private int canvasX;
	private int canvasY;

	public ConsoleCanvas(int arrayX, int arrayY) {
		canvasX = arrayX;
		canvasY = arrayY;
		canvas = new char[arrayX][arrayY];

		for (int i = 0; i < canvasX; i++) {
			for (int j = 0; j < canvasY; j++) {
				canvas[i][j] = ' ';
				if (i == 0) {
					canvas[i][j] = '#';
				}
				if (j == 0) {
					canvas[i][j] = '#';
				}
				if (j == arrayY - 1) {
					canvas[i][j] = '#';
				}
				if (i == arrayX - 1) {
					canvas[i][j] = '#';
				}

			}
		}

	}

	public void showIt(int droneX, int droneY, char ch) {

		canvas[droneX + 1][droneY + 1] = ch;
	}

	public String toString() {
		String res = "";
		for (int i = 0; i < canvasX; i++) {
			for (int j = 0; j < canvasY; j++) {
				res = res + canvas[i][j] + " ";
			}
			res = res + "\n";
		}
		return res;
	}

	public static void main(String[] args) {
		ConsoleCanvas c = new ConsoleCanvas(10, 20);
		System.out.println(c.toString());

	}
}

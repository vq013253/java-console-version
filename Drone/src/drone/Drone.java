package drone;

public class Drone {
	private int droneY, droneX;
	static int incrementor = 0;
	int id;
	private Direction dir;

	public Drone(int y, int x, Direction d) {
		droneX = x;
		droneY = y;
		id = incrementor++;
		dir = d;
	}

     //return attributes of the drones
	public int getx() {
		return droneX;

	}

	public int gety() {
		return droneY;
	}

	public Direction getDir() {
		return dir;
	}

	public String toString() {
		return String
				.format("Drone " + id + " is at " + droneY + " " + droneX + " with direction " + dir.toString() + "\n");

	}

	public boolean isHere(int sx, int sy) {
		if (droneX == sx && droneY == sy) {
			return true;
		} else {
			return false;
		}
	}

	public void displayDrone(ConsoleCanvas c) {
		char droneChr = 'D';
		c.showIt(droneX, droneY, droneChr);
		// << call the showIt method in c to put a D where the drone is
	}

	public void tryToMove(DroneArena a) {
		switch (dir) {
		case north:
			if (a.canMoveHere(droneX - 1, droneY)) // checks move eligibility
				droneX = droneX - 1; // drone moves
			else
				dir = dir.nextDirection(); // changes direction
			break;
		case east:

			if (a.canMoveHere(droneX, droneY + 1))
				droneY = droneY + 1;
			else
				dir = dir.nextDirection();
			break;
		case south:
			if (a.canMoveHere(droneX + 1, droneY))
				droneX = droneX + 1;
			else
				dir = dir.nextDirection();
			break;
		case west:
			if (a.canMoveHere(droneX, droneY - 1))
				droneY = droneY - 1;
			else
				dir = dir.nextDirection();
			break;
		default:
			break;
		}
	}
}

//public static void main(String[] args) {
// Drone d = new Drone(5, 3);
// System.out.println(d.toString());
//}

//}

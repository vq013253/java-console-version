package drone;

import java.util.Random;

public enum Direction {
	south, west, north, east;

	public static Direction randomDirection() {
		Random random = new Random();
		return values()[random.nextInt(values().length)];
	}

	public Direction nextDirection() {
		if (this.ordinal() == 3)
			return values()[0];
		else
			return values()[this.ordinal() + 1];
	}

	// public static void main(String[] args) {//

	// Direction dir = Direction.randomDirection();
	// System.out.println(dir.toString());
	// System.out.println(dir.nextDirection());

	// }
}
